@extends('layout.master')

@section('content')

  <div class="w-50 mx-auto pt-5 align-middle col-center">
    <h1 class=" pb-5 text-center display-4">Laravel desde cero</h1>

    <div class="row text-center pt-5 ">
      <div class="col-sm  center-block">
        <p><a href="https://laravel.com/" class="text-info">Laravel</a></p>
      </div>
      <div class="col-sm">
        <p><a href="http://getbootstrap.com/" class="text-info">Bootstrap</a></p>
      </div>
      <div class="col-sm">
        <p><a href="https://goo.gl/CLZ3pA" class="text-info">Curso</a></p>
      </div>
      <div class="col-sm">
        <p><a href="#" class="text-info">@xochescam</a></p>
      </div>
    </div>
  </div>

@endsection