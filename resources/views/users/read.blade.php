@extends('layout.master')

@section('content')

  <h2 class="p-t text-center">Ver usuario</h2>
  <hr class="mb-5">

  <div class="card w-50 bg-light mx-auto">
    <div class="card-body">
      <p class="font-weight-bold">Nombre:</p>
      <p class="card-text">{{ $user->name }}</p>

      <p class="font-weight-bold">Email:</p>
      <p class="card-text">{{ $user->email }}</p>

      <p class="font-weight-bold">Edad:</p>
      <p class="card-text">{{ $user->age }}</p>

      <p class="font-weight-bold">Carrera:</p>
      <p class="card-text">{{ $user->career->name }}</p>
    </div>
  </div>

@endsection