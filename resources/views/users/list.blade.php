@extends('layout.master')

@section('content')

  <h2 class="p-t text-center">Lista de usuarios</h2>
  <hr class="mb-5">

  @include('alerts.success')

  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Correo</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
        <tr>
          <th>{{ $user->name }}</th>
          <td>{{ $user->email }}</td>
          <td class="text-center">
            <a href="{{ route('user.show',$user->id) }}" class="btn btn-success " role="button" aria-pressed="true" >Ver</a>
            <a href="{{ route('user.edit',$user->id) }}" class="btn btn-info" role="button" aria-pressed="true">Editar</a>
            <a href="{{ route('user.delete', $user->id) }}" class="btn btn-danger" role="button" aria-pressed="true">Eliminar</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

@endsection

