<input name="_token" type="hidden" value="{{ csrf_token() }}">

<div class="form-group row">
  <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
  <div class="col-sm-10">
    <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" value="{{ isset($user->name) ? $user->name : old('name') }}">
  </div>
</div>

<div class="form-group row">
  <label for="email" class="col-sm-2 col-form-label">E-mail:</label>
  <div class="col-sm-10">
    <input type="email" name="email" class="form-control" id="email" placeholder="ejemplo@email.com" value="{{ isset($user->email) ? $user->email : old('email') }} ">
  </div>
</div>

<div class="form-group row">
  <label for="age" class="col-sm-2 col-form-label">Edad:</label>
  <div class="col-sm-10">
    <input type="number" name="age" class="form-control" id="age" value="{{ isset($user->age) ? $user->age : old('age') }}">
  </div>
</div>

<div class="form-group row">
  <label for="career_id" class="col-sm-2 col-form-label">Carrera:</label>
  <div class="col-sm-10">
    <select class="form-control" name="career_id" id="career_id">
      <option value="">Seleccionar</option>
      @foreach($careers as $career)

        @if( (isset($user->age) && $user->career_id == $career->id) || (old('age') == $career->id) )
          <option value="{{ $career->id }}" selected>{{ $career->name }}</option>
        @else
          <option value="{{ $career->id }}">{{ $career->name }}</option>
        @endif

      @endforeach
    </select>
  </div>
</div>

<div class="form-group row">
  <div class="offset-sm-2 col-sm-10">
    <button type="submit" class="btn btn-primary">Guardar</button>
  </div>
</div>