@extends('layout.master')

@section('content')

  <h2 class="p-t text-center">Editar</h2>
  <hr class="mb-5">

  @include('alerts.error')
  @include('alerts.success')
  @include('alerts.warning')

  <div class="card w-75 bg-light mx-auto">
    <div class="card-body">
      <form method="POST" action="{{ route('user.update',$user->id) }}">
        @include('users.form')
      </form>
    </div>
  </div>

@endsection