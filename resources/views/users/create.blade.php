@extends('layout.master')

@section('content')

  <h2 class="p-t text-center">Crear usuario</h2>
  <hr class="mb-5">

  @include('alerts.error')
  @include('alerts.success')
  @include('alerts.warning')

  <div class="card w-75 bg-light mx-auto">
    <div class="card-body">
      <form method="POST" action="{{ url('/user') }}">
        @include('users.form')
      </form>
    </div>
  </div>

@endsection