<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = [
        'name', 'career_id'
    ];

      /**
     * Get the user that owns the career.
     */
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
