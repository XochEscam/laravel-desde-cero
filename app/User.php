<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'age',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

     /**
     * Get the career record associated with the user.
     */
    public function career()
    {
        return $this->belongsTo('App\Career');
    }

    /**
     * Save data from user
     */
    public static function saveData($request)
    {
        $user = new User;

        $user->name      = $request->name;
        $user->email     = $request->email;
        $user->age       = $request->age;
        $user->career_id = $request->career_id;
        $user->save();

        return $user;
    }

    /**
     * Update data from user
     */
    public static function updateData($request, $id)
    {
        $user = User::findOrfail($id);

        $user->name      = $request->name;
        $user->email     = $request->email;
        $user->age       = $request->age;
        $user->career_id = $request->career_id;
        $user->save();

        return $user;
    }
}
